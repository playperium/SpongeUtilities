package eu.playperium.spongeutilities;

import com.google.inject.Inject;
import eu.playperium.spongecore.configuration.ConfigurationManager;
import eu.playperium.spongeutilities.commands.CommandSetSpawn;
import eu.playperium.spongeutilities.commands.CommandSpawn;
import eu.playperium.spongeutilities.configuration.PlayerDataManager;
import eu.playperium.spongeutilities.configuration.PluginConfig;
import eu.playperium.spongeutilities.database.ConnectionPool;
import eu.playperium.spongeutilities.listeners.ConnectionListener;
import eu.playperium.spongeutilities.services.Services;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;

import java.io.File;

@Plugin(
        id = "spongeutilities",
        name = "SpongeUtilities",
        dependencies = {@Dependency(id = "spongecore")}
)
public class SpongeUtilities {

    @Inject
    private Logger logger;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private File configurationFile = null;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configurationLoader = null;

    public ConfigurationManager configurationManager;
    public PluginConfig pluginConfig;

    public PlayerDataManager playerDataManager;
    public ConnectionPool connectionPool;
    public Services services;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        configurationManager = new ConfigurationManager(configurationFile, configurationLoader);
        pluginConfig = new PluginConfig(configurationManager);

        playerDataManager = new PlayerDataManager(this);
        connectionPool = new ConnectionPool(this);

        if (this.configurationManager.getBoolean(PluginConfig.serverMysqlEnabled)) {
            this.connectionPool.initialize();
        }

        services = new Services(this);

        registerListeners();
        registerCommands();
    }

    private void registerListeners() {
        Sponge.getEventManager().registerListeners(this, new ConnectionListener(this));
    }

    private void registerCommands() {
        Sponge.getCommandManager().register(this, new CommandSetSpawn(this).commandSpec, "setspawn");
        Sponge.getCommandManager().register(this, new CommandSpawn(this).commandSpec, "spawn");
    }
}
