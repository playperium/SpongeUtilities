package eu.playperium.spongeutilities.inventory;

import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;

public final class HotbarItems {

    public static final ItemStack protectionTool() {
        ItemStack protectionTool = ItemStack.builder().itemType(ItemTypes.GOLDEN_SHOVEL).build();

        return protectionTool;
    }

    public static final ItemStack bread() {
        ItemStack bread = ItemStack.builder().itemType(ItemTypes.BREAD).quantity(16).build();

        return bread;
    }

    public static final ItemStack chest() {
        ItemStack chest = ItemStack.builder().itemType(ItemTypes.CHEST).build();

        return chest;
    }
}
