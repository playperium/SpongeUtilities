package eu.playperium.spongeutilities.services;

import eu.playperium.spongecore.service.InternalService;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.title.Title;

import java.util.concurrent.TimeUnit;

public class RubbishCollection implements InternalService {

    private eu.playperium.spongeutilities.SpongeUtilities plugin;

    public RubbishCollection(eu.playperium.spongeutilities.SpongeUtilities spongeUtilities) {
        this.plugin = spongeUtilities;
    }

    @Override
    public void execute() {
        Task.Builder builder = Task.builder();

        builder.execute(
                task -> {
                    for (Player player : Sponge.getServer().getOnlinePlayers()) {
                        player.sendTitle(Title.builder().title(Text.builder("Müllabfuhr").color(TextColors.GOLD).append().build())
                                .subtitle(Text.builder("In einer Minuten werden alle herumliegenden Items gelöscht!").color(TextColors.GRAY).build()).build());
                    }

                    builder.execute(
                            task1 -> {
                                for (Player player : Sponge.getServer().getOnlinePlayers()) {
                                    player.sendTitle(getRubbishMessage(TimeUnit.SECONDS, 10));
                                }

                                builder.execute(
                                        task2 -> {
                                            Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "kill @e[type=item]");

                                            Title rubbishTitle = Title.builder().title(Text.builder("Müllabfuhr").color(TextColors.GOLD).append().build())
                                                    .subtitle(Text.builder("Alle herumliegenden Items wurden gelöscht!").color(TextColors.GRAY).build()).build();

                                            for (Player player : Sponge.getServer().getOnlinePlayers()) {
                                                player.sendTitle(rubbishTitle);
                                            }
                                        }
                                ).delay(10, TimeUnit.SECONDS).submit(plugin);
                            }
                    ).delay(1, TimeUnit.MINUTES).submit(plugin);
                }
        ).interval(30, TimeUnit.MINUTES).submit(plugin);
    }

    private Title getRubbishMessage(TimeUnit unit, int time) {
        if (unit == TimeUnit.SECONDS) {
            return Title.builder().title(Text.builder("Müllabfuhr").color(TextColors.GOLD).append().build())
                    .subtitle(Text.builder("In " + time + " Sekunden werden alle herumliegenden Items gelöscht!").color(TextColors.GRAY).build()).build();
        }

        return null;
    }
}
