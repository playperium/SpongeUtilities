package eu.playperium.spongeutilities.services;

import eu.playperium.spongecore.service.ServiceManager;
import eu.playperium.spongeutilities.configuration.PluginConfig;

public class Services {

    private eu.playperium.spongeutilities.SpongeUtilities plugin;

    public Services(eu.playperium.spongeutilities.SpongeUtilities spongeUtilities) {
        this.plugin = spongeUtilities;
        ServiceManager serviceManager = new ServiceManager();

        if(plugin.configurationManager.getBoolean(PluginConfig.serverServicesServerRestart)) {
            System.out.println("Starting ServerRestart Service...");
            serviceManager.registerInternalService(new ServerRestart(plugin));
        }

        if(plugin.configurationManager.getBoolean(PluginConfig.serverServicesServerRubbishCollection)) {
            System.out.println("Starting RubbishCollection Service...");
            serviceManager.registerInternalService(new RubbishCollection(plugin));
        }

        serviceManager.loadServices();
    }
}
