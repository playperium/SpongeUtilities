package eu.playperium.spongeutilities.services;

import eu.playperium.spongecore.service.InternalService;
import eu.playperium.spongeutilities.configuration.PluginConfig;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.title.Title;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ServerRestart implements InternalService {

    private eu.playperium.spongeutilities.SpongeUtilities plugin;

    public ServerRestart(eu.playperium.spongeutilities.SpongeUtilities spongeUtilities) {
        this.plugin = spongeUtilities;
    }

    private LocalTime currentTime = LocalTime.now();

    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private Date date = new Date();
    private String currentDate = dateFormat.format(date);

    @Override
    public void execute() {
        Task.Builder builder = Task.builder();
        LocalTime restartTime = LocalTime.parse("04:00");

        String lastRestart = plugin.configurationManager.getString(PluginConfig.serverLastRestart);

        builder.execute(
                task -> {

                    currentTime = LocalTime.now();

                    if (currentTime.equals(restartTime) || currentTime.getHour() == restartTime.getHour() && currentTime.getMinute() > restartTime.getMinute()) {
                        if (!currentDate.equals(lastRestart)) {

                            restartServer();
                        }
                    }
                }
        ).interval(15, TimeUnit.MINUTES).submit(plugin);
    }

    private void restartServer() {
        if (Sponge.getServer().getOnlinePlayers().size() != 0) {
            plugin.configurationManager.setValue(PluginConfig.serverLastRestart, currentDate);

            for (Player player : Sponge.getServer().getOnlinePlayers()) {
                player.sendTitle(getRestartMessage(TimeUnit.MINUTES, 5));
            }

            Task.Builder builder = Task.builder();
            builder.execute(
                    task -> {
                        for (Player player : Sponge.getServer().getOnlinePlayers()) {
                            player.sendTitle(getRestartMessage(TimeUnit.SECONDS, 10));
                        }

                        builder.execute(
                                task1 -> {
                                    Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "save-all");

                                    Text kickMessage = Text.builder("Server is restarting...").toText();
                                    Sponge.getServer().shutdown(kickMessage);
                                }
                        ).delay(10, TimeUnit.SECONDS).submit(plugin);
                    }

            ).delay(5, TimeUnit.MINUTES).submit(plugin);
        } else {
            Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "save-all");

            Text kickMessage = Text.builder("Server is restarting...").toText();
            Sponge.getServer().shutdown(kickMessage);
        }
    }

    private Title getRestartMessage(TimeUnit unit, int time) {
        if (unit == TimeUnit.MINUTES) {
            Text restartMessage = Text.builder("Der Server startet in " + time + " Minuten neu!").color(TextColors.RED).build();

            return Title.builder().title(restartMessage).build();
        }

        if (unit == TimeUnit.SECONDS) {
            Text restartMessage = Text.builder("Der Server start in " + time + " Sekunden neu!").color(TextColors.RED).build();

            return Title.builder().title(restartMessage).build();
        }

        return null;
    }
}
