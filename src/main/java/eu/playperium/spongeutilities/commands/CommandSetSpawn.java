package eu.playperium.spongeutilities.commands;

import eu.playperium.spongeutilities.configuration.PluginConfig;
import eu.playperium.spongeutilities.permissions.PluginPermissions;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.chat.ChatTypes;

public class CommandSetSpawn implements CommandExecutor {

    private eu.playperium.spongeutilities.SpongeUtilities plugin;

    public CommandSetSpawn(eu.playperium.spongeutilities.SpongeUtilities spongeUtilities) {
        this.plugin = spongeUtilities;
    }

    public CommandSpec commandSpec = CommandSpec.builder().permission(PluginPermissions.COMMAND_SET_SPAWN).executor(this).build();

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof Player) {
            Player player = (Player) src;

            String worldname = Sponge.getServer().getDefaultWorldName();

            double x = player.getLocation().getX();
            double y = player.getLocation().getY();
            double z = player.getLocation().getZ();

            double rotX = player.getRotation().getX();
            double rotY = player.getRotation().getY();
            double rotZ = player.getRotation().getZ();

            plugin.configurationManager.setValue(PluginConfig.serverSpawnWorld, worldname);

            plugin.configurationManager.setValue(PluginConfig.serverSpawnX, x);
            plugin.configurationManager.setValue(PluginConfig.serverSpawnY, y);
            plugin.configurationManager.setValue(PluginConfig.serverSpawnZ, z);

            plugin.configurationManager.setValue(PluginConfig.serverSpawnRotationX, rotX);
            plugin.configurationManager.setValue(PluginConfig.serverSpawnRotationY, rotY);
            plugin.configurationManager.setValue(PluginConfig.serverSpawnRotationZ, rotZ);

            player.sendMessage(ChatTypes.CHAT, Text.builder("Spawn location set!").build());
        }

        return CommandResult.success();
    }
}
