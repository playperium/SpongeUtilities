package eu.playperium.spongeutilities.commands;

import com.flowpowered.math.vector.Vector3d;
import eu.playperium.spongeutilities.configuration.PluginConfig;
import eu.playperium.spongeutilities.permissions.PluginPermissions;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class CommandSpawn implements CommandExecutor {

    private eu.playperium.spongeutilities.SpongeUtilities plugin;

    public CommandSpawn (eu.playperium.spongeutilities.SpongeUtilities spongeUtilities) {
        this.plugin = spongeUtilities;
    }

    public CommandSpec commandSpec = CommandSpec.builder().permission(PluginPermissions.COMMAND_SPAWN).executor(this).build();

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof Player) {
            Player player = (Player) src;

            String worldname = plugin.configurationManager.getString(PluginConfig.serverSpawnWorld);

            double x = plugin.configurationManager.getDouble(PluginConfig.serverSpawnX);
            double y = plugin.configurationManager.getDouble(PluginConfig.serverSpawnY);
            double z = plugin.configurationManager.getDouble(PluginConfig.serverSpawnZ);

            double rotX = plugin.configurationManager.getDouble(PluginConfig.serverSpawnRotationX);
            double rotY = plugin.configurationManager.getDouble(PluginConfig.serverSpawnRotationY);
            double rotZ = plugin.configurationManager.getDouble(PluginConfig.serverSpawnRotationZ);

            World world = Sponge.getServer().getWorld(worldname).get();

            Location<World> location = new Location(world, x, y, z);
            Vector3d rotation = new Vector3d(rotX, rotY, rotZ);

            player.setLocation(location);
            player.setRotation(rotation);
        }

        return CommandResult.success();
    }
}
