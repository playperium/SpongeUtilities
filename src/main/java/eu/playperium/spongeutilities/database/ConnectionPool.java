package eu.playperium.spongeutilities.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import eu.playperium.spongeutilities.configuration.PluginConfig;

import java.sql.*;

public class ConnectionPool {

    private String host;
    private int port;

    private String databaseName;

    private String user;
    private String password;

    private HikariDataSource dataSource;

    private eu.playperium.spongeutilities.SpongeUtilities plugin;

    public ConnectionPool(eu.playperium.spongeutilities.SpongeUtilities spongeUtilities) {
        this.plugin = spongeUtilities;
    }

    public void initialize() {
        this.host = plugin.configurationManager.getString(PluginConfig.serverMysqlHost);
        this.port = plugin.configurationManager.getInt(PluginConfig.serverMysqlPort);

        this.databaseName = plugin.configurationManager.getString(PluginConfig.serverMysqlDatabase);

        this.user = plugin.configurationManager.getString(PluginConfig.serverMysqlUser);
        this.password = plugin.configurationManager.getString(PluginConfig.serverMysqlPassword);

        this.dataSource = this.getDataSource();
    }

    public HikariDataSource getDataSource() {
        HikariConfig config = new HikariConfig();

        config.setMaximumPoolSize(10);
        config.setMinimumIdle(2);
        config.setJdbcUrl("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.databaseName);
        config.setUsername(this.user);
        config.setPassword(this.password);

        return new HikariDataSource(config);
    }

    public ResultSet executeQuery(String sql) throws SQLException {

        Connection connection = this.dataSource.getConnection();
        ResultSet resultSet = null;

        try {
            resultSet = connection.createStatement().executeQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return resultSet;
    }
}
