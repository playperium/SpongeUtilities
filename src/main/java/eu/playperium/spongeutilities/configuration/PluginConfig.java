package eu.playperium.spongeutilities.configuration;

import eu.playperium.spongecore.configuration.ConfigOption;
import eu.playperium.spongecore.configuration.ConfigurationManager;

public class PluginConfig {

    private ConfigurationManager configurationManager;

    public static ConfigOption serverName = new ConfigOption("server", "server", "name");

    public static ConfigOption serverSpawnWorld = new ConfigOption(null, "server", "spawn", "world");
    public static ConfigOption serverSpawnX = new ConfigOption(null, "server", "spawn", "x");
    public static ConfigOption serverSpawnY = new ConfigOption(null, "server", "spawn", "y");
    public static ConfigOption serverSpawnZ = new ConfigOption(null, "server", "spawn", "z");
    public static ConfigOption serverSpawnRotationX = new ConfigOption(null, "server", "spawn", "rotation", "x");
    public static ConfigOption serverSpawnRotationY = new ConfigOption(null, "server", "spawn", "rotation", "y");
    public static ConfigOption serverSpawnRotationZ = new ConfigOption(null, "server", "spawn", "rotation", "z");

    public static ConfigOption serverMysqlEnabled = new ConfigOption(false, "server", "mysql", "enabled");
    public static ConfigOption serverMysqlHost = new ConfigOption("127.0.0.1", "server", "mysql", "host");
    public static ConfigOption serverMysqlPort = new ConfigOption(3306, "server", "mysql", "port");
    public static ConfigOption serverMysqlDatabase = new ConfigOption("SpongeUtilities", "server", "mysql", "database");
    public static ConfigOption serverMysqlUser = new ConfigOption("username", "server", "mysql", "user");
    public static ConfigOption serverMysqlPassword = new ConfigOption("password", "server", "mysql", "password");

    public static ConfigOption serverLastRestart = new ConfigOption("01/01/2018", "server", "lastRestart");

    public static ConfigOption serverServicesServerRestart = new ConfigOption(false, "server", "services", "serverRestart");
    public static ConfigOption serverServicesServerRubbishCollection = new ConfigOption(false, "server", "services", "rubbishCollection");

    public PluginConfig(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;

        registerConfigOptions();
    }

    private void registerConfigOptions() {
        configurationManager.registerConfigOption(serverName);

        configurationManager.registerConfigOption(serverSpawnWorld);
        configurationManager.registerConfigOption(serverSpawnX);
        configurationManager.registerConfigOption(serverSpawnY);
        configurationManager.registerConfigOption(serverSpawnZ);
        configurationManager.registerConfigOption(serverSpawnRotationX);
        configurationManager.registerConfigOption(serverSpawnRotationY);
        configurationManager.registerConfigOption(serverSpawnRotationZ);

        configurationManager.registerConfigOption(serverMysqlEnabled);
        configurationManager.registerConfigOption(serverMysqlHost);
        configurationManager.registerConfigOption(serverMysqlPort);
        configurationManager.registerConfigOption(serverMysqlDatabase);
        configurationManager.registerConfigOption(serverMysqlUser);
        configurationManager.registerConfigOption(serverMysqlPassword);

        configurationManager.registerConfigOption(serverLastRestart);

        configurationManager.registerConfigOption(serverServicesServerRestart);
        configurationManager.registerConfigOption(serverServicesServerRubbishCollection);

        configurationManager.setConfigOptions();
    }
}
