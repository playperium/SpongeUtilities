package eu.playperium.spongeutilities.configuration;

import java.io.File;
import java.nio.file.Path;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class PlayerDataManager {

    private Path dataPath;
    private File filePath;

    private eu.playperium.spongeutilities.SpongeUtilities plugin;

    public PlayerDataManager(eu.playperium.spongeutilities.SpongeUtilities spongeUtilities) {
        this.plugin = spongeUtilities;
    }

    public void initialize() {
        try {
            if (!filePath.exists()) {
                filePath.mkdirs();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean playerExists(UUID uuid) {
        boolean playerExists = false;

        String serverName = plugin.configurationManager.getString(PluginConfig.serverName);
        System.out.println("SERVER NAME: " + serverName);

        try {
            ResultSet resultSet = plugin.connectionPool.executeQuery("select * from PlayerData where ServerName like '" + serverName + "' and PlayerUUID like '" + uuid.toString() + "'");

            while (resultSet.next()) {
                playerExists = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return playerExists;
    }

    public void createPlayer(UUID uuid) {

        String serverName = plugin.configurationManager.getString(PluginConfig.serverName);
        System.out.println("SERVER NAME: " + serverName);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        try {
            ResultSet resultSet = plugin.connectionPool.executeQuery("insert into PlayerData (ServerName, PlayerUUID, FirstJoinDate, ReceivedDefaultKit) " +
                    "values ('" + serverName + "', '" + uuid.toString() + "', '" + dateFormat.format(date) + "', 'false')");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playerGetDefaultKit(UUID uuid) {

        String serverName = plugin.configurationManager.getString(PluginConfig.serverName);
        System.out.println("SERVER NAME: " + serverName);

        try {
            ResultSet resultSet = plugin.connectionPool.executeQuery("update PlayerData set ReceivedDefaultKit = 'true' where ServerName like '" +
                    serverName + "' and PlayerUUID like '" + uuid.toString() + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
