package eu.playperium.spongeutilities.listeners;

import eu.playperium.spongeutilities.inventory.HotbarItems;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.item.inventory.entity.Hotbar;
import org.spongepowered.api.item.inventory.property.SlotIndex;

import java.lang.annotation.Annotation;

public class ConnectionListener implements Listener {

    private eu.playperium.spongeutilities.SpongeUtilities plugin;

    public ConnectionListener (eu.playperium.spongeutilities.SpongeUtilities spongeUtilities) {
        this.plugin = spongeUtilities;
    }

    @Override
    public Order order() {
        return null;
    }

    @Override
    public boolean beforeModifications() {
        return false;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }

    @Listener
    public void onClientJoin(ClientConnectionEvent.Join e) {
        Player player = e.getCause().first(Player.class).get();

        if (!plugin.playerDataManager.playerExists(player.getUniqueId())) {
            plugin.playerDataManager.createPlayer(player.getUniqueId());

            if (!player.hasPlayedBefore()) {
                setPlayerHotbar(player);
                Sponge.getCommandManager().process(player, "spawn");
            }
        }
    }

    private void setPlayerHotbar(Player player) {
        Hotbar hotbar = player.getInventory().query(Hotbar.class);

        hotbar.set(new SlotIndex(6), HotbarItems.protectionTool());
        hotbar.set(new SlotIndex(7), HotbarItems.chest());
        hotbar.set(new SlotIndex(8), HotbarItems.bread());

        plugin.playerDataManager.playerGetDefaultKit(player.getUniqueId());
    }


}
