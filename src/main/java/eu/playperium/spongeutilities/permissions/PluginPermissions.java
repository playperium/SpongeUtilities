package eu.playperium.spongeutilities.permissions;

public final class PluginPermissions {

    // Command Permissions
    public static final String COMMAND_SET_SPAWN = "pp.spongeutilities.command.setspawn";
    public static final String COMMAND_SPAWN = "pp.spongeutilities.command.spawn";
}
